GameManager.Level_1.prototype = {

    preload: function () {
        game.load.image('ball', 'asset/main/ball.png');
        game.load.image('main', 'asset/main/main.png');
        game.load.image('circle', 'asset/main/circle.png');
        game.load.image('replayButton', 'asset/main/back.png');
        game.load.image('highscoreIcon', 'asset/main/highscore.png');
        game.add.text(0, 0, "hack", {font:"1px roboto", fill:"#FFFFFF"})
    },
    create: function () {
        //background
        game.add.image(0, 0, 'background2');
        var back = game.add.sprite(0, 0, 'background1');
        back.alpha = 0;
        var backTween = game.add.tween(back).to( { alpha: 1 }, 5000, "Linear", true, 0, -1);
        backTween.yoyo(true, 0);

        //particles
        emitter = game.add.emitter(game.world.centerX, game.world.centerY*2, 200);
        emitter.makeParticles('particle');
        emitter.width = game.world.width;
        emitter.setRotation(0, 90);
        emitter.setAlpha(0.1, 0.5);
        emitter.setScale(0.5, 1);
        emitter.setYSpeed(5, 15);
        emitter.gravity = -100;
        emitter.start(false, 5000, 100);

        //middle circle
        circle = game.add.sprite(0, 0, 'circle');
        circle.position.x = game.world.width/2 - circle.width/2;
        circle.position.y = game.world.height/2 - circle.height/2;
        circle.tint = mainBallColors[level][0];

        //middle ball
        mainBall = game.add.sprite(0, 0, 'main');
        mainBall.anchor.set(0.5, 0.5);
        mainBall.position.x = game.world.width/2;
        mainBall.position.y = game.world.height/2;
        mainBall.tint = mainBallColors[level][0];
        game.physics.arcade.enable(mainBall);
        mainBall.body.immovable = true;
        //todo: circle collision fix
        mainBall.body.setCircle(35);

        //input
        var space = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        space.onDown.add(this.swapColors, mainBall);

        //small
        smallBalls = game.add.group();

        //timer
        //game.time.events.repeat(Phaser.Timer.SECOND * 2, 10, this.spawnBall, this);

        timer = game.time.create(false);
        timer.loop(ballSpawnTimer, this.spawnBall, this);
        timer.start();
        ballCounter = 0;

        //ui
        replayButton = game.add.button(game.world.centerX-20, game.world.centerY-20, 'replayButton', this.actionOnClick, this, 1, 0, 0);
        level_ui_group = game.add.group();
        level_ui_group.add(replayButton);
        level_ui_group.visible = false;

        //current score
        var style = {font: "55px roboto", fill: "#888888"};
        score = game.add.text(game.world.centerX - 15,32,"0", style);
        ballsDestroyed = 0;

        //highscore
        highscore = 0;
        highscoreIcon = game.add.sprite(20, game.world.height - 75, 'highscoreIcon');
        highscoreIcon.scale.setTo(0.2,0.2);
        style = {font: "25px roboto", fill: "#aaaaaa"};
        highscoreText = game.add.text(75,game.world.height -75,"0", style);

    },
    update: function () {
        //detect collision
        game.physics.arcade.collide(mainBall, smallBalls, this.collisionHandler, null, this);
        score.text = ballsDestroyed;
        highscoreText.text = highscore;
    },
    actionOnClick: function(e) {
        if (e.key == 'replayButton') {
            //todo: restart game
            this.restartGame();
        }
    },
    restartGame: function() {
        console.log('restart');
        level_ui_group.visible = false;

        level = 0;
        ballCounter = 0;
        ballsDestroyed = 0;
        smallBalls = game.add.group();
        //main ball + circle
        mainBallState = 0;
        mainBall.position.x = game.world.width/2;
        mainBall.position.y = game.world.height/2;
        mainBall.tint = mainBallColors[level][0];
        circle.tint = mainBallColors[level][0];

        //reset counters
        timer = game.time.create(false);
        timer.loop(ballSpawnTimer, this.spawnBall, this);
        timer.start();
    },
    nextLevel: function() {
        timer.loop(ballSpawnTimer, this.spawnBall, this);
        timer.start();
        level++;
        mainBallState = 0;
        mainBall.tint = mainBallColors[level][0];
        ballCounter = 0;
        if (level > levelMax)
        {
            level = 0;
        }
    },
    swapColors: function (ball) {
        console.log('swap');
        if (mainBallState == 0)
            mainBallState = 1;
        else
            mainBallState = 0;
        mainBall.tint = mainBallColors[level][mainBallState];
    },
    collisionHandler: function (obj1, obj2) {
        if (obj2.tint != obj1.tint) {
            game.camera.shake(0.01, cameraShakeMagnitude);

            emitter = game.add.emitter(obj1.x+35, obj1.y+35, 50);
            emitter.makeParticles('ball');
            emitter.forEach(function(particle) {  particle.tint = obj1.tint;});
            emitter.setAlpha(1, 0.1, 3000);
            emitter.setScale(1, 0.1, 1, 0.1, 6000, Phaser.Easing.Quintic.Out);
            emitter.start(true, 3000, null, 50);

            //todo: destroy all balls on map
            //smallBalls.destroy();
            level_ui_group.visible = true;
            mainBall.position.x = -10000;
            mainBall.position.y = -10000;
            timer.stop();
            if (ballsDestroyed > highscore)
                highscore = ballsDestroyed;
            while(smallBalls.children.length >0){
                smallBalls.children[0].destroy();
            }
            // smallBalls.children.forEach(ball => {
            //     console.log(ball);
            //     //smallBalls.remove(ball);
            //     ball.destroy();
            //     // var emitter = game.add.emitter(ball.x+35, ball.y+35, 50);
            //     // emitter.makeParticles('ball');
            //     // emitter.forEach(function(particle) {  particle.tint = ball.tint;});
            //     // emitter.setAlpha(1, 0.1, 3000);
            //     // emitter.setScale(1, 0.1, 1, 0.1, 6000, Phaser.Easing.Quintic.Out);
            //     // emitter.start(true, 3000, null, 50);
            // });
        }
        else {
            ballsDestroyed++;
        }
        
        obj2.destroy();

        //particle on hit
        emitter = game.add.emitter(obj2.x+20, obj2.y+20, 50);
        emitter.makeParticles('ball');
        emitter.forEach(function(particle) {  particle.tint = obj2.tint;});
        emitter.setAlpha(1, 0.1, 1500);
        emitter.setScale(0.5, 0.1, 0.5, 0.1, 3000, Phaser.Easing.Quintic.Out);
        emitter.start(true, 2000, null, 10);
    },
    spawnBall: function () {
        ballCounter++;
        if (ballCounter == ballsPerLevel)
        {
            circle.tint = mainBallColors[level+1][0];
            timer.stop();
            game.time.events.add(Phaser.Timer.SECOND * secondsBetweenLevels, this.nextLevel, this);
            return;
        }
        var r = game.rnd.integerInRange(0, 3);
        var x;
        var y;
        if (r == 0) {
            x = game.rnd.integerInRange(0, game.world.width);
            y = 0;
        }
        if (r == 1) {
            x = 0;
            y = game.rnd.integerInRange(0, game.world.height);
        }
        if (r == 2) {
            x = game.world.width;
            y = game.rnd.integerInRange(0, game.world.height);
        }
        if (r == 3) {
            x = game.rnd.integerInRange(0, game.world.width);
            y = game.world.height;
        }

        var tmp = game.add.sprite(x, y, 'ball');
        game.physics.arcade.enable(tmp);
        tmp.body.setCircle(25);
        //todo: adjust speed
        game.physics.arcade.moveToXY(tmp, mainBall.x, mainBall.y, 0.005, 1000);
        smallBalls.add(tmp);
        var r2 = game.rnd.integerInRange(0, 1);
        tmp.tint = mainBallColors[level][r2];

        //todo: particle trace line
    }
};