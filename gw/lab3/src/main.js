var GameManager = {};

GameManager.Main = function (game) {};
GameManager.Level_1 = function (game) {};

GameManager.Main.prototype = {
    preload: function () {
        game.load.spritesheet('new_game_button', 'asset/new_game_button2.png', 596/2, 33);
        game.load.spritesheet('settings_button', 'asset/settings_button.png', 596/2, 33);
        game.load.spritesheet('back_button', 'asset/back_button.png', 214/2, 35);
        game.load.image('frame', 'asset/frame.png');
        game.load.image('background1', 'asset/menu_background.png');
        game.load.image('background2', 'asset/menu_background2.png');
        game.load.image('particle', 'asset/particle.png');
    },
    create: function () {
        //game.input.setDefaultCursor('url(asset/mouse.png), pointer');
        
        //background
        game.stage.backgroundColor = "#000000";
        game.add.image(0, 0, 'background2');
        var back = game.add.sprite(0, 0, 'background1');
        back.alpha = 0;
        var backTween = game.add.tween(back).to( { alpha: 1 }, 5000, "Linear", true, 0, -1);
        backTween.yoyo(true, 0);
    
        //buttons
        main_menu_button_group = game.add.group();
        settings_menu_button_group = game.add.group();

        game.add.image(game.world.centerX/2, game.world.centerY/2, 'frame');
        
        new_button = game.add.button(game.world.centerX - 182, game.world.centerY - 50, 'new_game_button', this.actionOnClick, this, 1, 0, 0);
        settigs_button = game.add.button(game.world.centerX - 182, game.world.centerY, 'settings_button', this.actionOnClick, this, 1, 0, 0);
        back_button = game.add.button(game.world.centerX + 10, game.world.centerY + 135, 'back_button', this.actionOnClick, this, 1, 0, 0);
        main_menu_button_group.add(new_button);
        main_menu_button_group.add(settigs_button);
        settings_menu_button_group.add(back_button);
        settings_menu_button_group.visible = false;

        //particle
        emitter = game.add.emitter(game.world.centerX, game.world.centerY*2, 200);
        emitter.makeParticles('particle');
        emitter.width = game.world.width;
        emitter.setRotation(0, 90);
        emitter.setAlpha(0.1, 0.5);
        emitter.setScale(0.5, 1);
        emitter.setYSpeed(5, 15);
        emitter.gravity = -100;
        emitter.start(false, 5000, 100);
    },
    update: function () { },
    actionOnClick: function (e) {
        console.log(e.key);
        if (e.key == 'new_game_button') {
            this.state.start("Level_1");
        }
        if (e.key == 'settings_button') {    
            settings_menu_button_group.visible = true;
            main_menu_button_group.visible = false;
        }
        if (e.key == 'back_button') {
            settings_menu_button_group.visible = false;
            main_menu_button_group.visible = true;
        }
    }
};