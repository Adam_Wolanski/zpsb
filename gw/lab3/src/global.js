var mainBall;
var mainBallColors = [
    ['0x27ae60', '0x3498db'],
    ['0x9b59b6', '0xe67e22'],
    ['0xc0392b', '0xecf0f1'],
    ['0x16a085', '0x2c3e50'],
    ['0xf1c40f', '0x27ae60']
];

var smallBalls;
var timer;

var mainBallState = 0;
var level = 0;
var levelMax = 4;
var points;
var score = 0;
var ballCounter = 0;
var circle;
var ballsDestroyed = 0;

//TIMERS
var ballsPerLevel = 10;
var ballSpawnTimer = 500;
var secondsBetweenLevels = 3;
var cameraShakeMagnitude = 100;