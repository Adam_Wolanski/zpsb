package weather;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.io.*;
import org.json.*;

public class UrlWeather {
    private String _rawUrl, _key, _location;
    private HttpURLConnection _connection;
    private URL _url;

    public UrlWeather(String rawUrl, String key, String loc, String days) {
        _rawUrl = rawUrl;
        _key = key;
        _location = loc;

        try {
            _url = UrlParser.GetUrl(rawUrl, key, loc, days);
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Invalid URL!");
        }

    }

    public void Connect() {
        WeatherDebug.DebugTrace();

        try {
            _connection = (HttpURLConnection) _url.openConnection();
            _connection.setRequestMethod("GET");
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Cannot connect to URL!");
        }

    }

    public String GetResponse() {
        WeatherDebug.DebugTrace();

        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(_connection.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            return content.toString();
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Cannot get response!");
            System.exit(-1);
        }

        return null;
    }

    public void Disconnect() {
        WeatherDebug.DebugTrace();

        _connection.disconnect();
    }

    public static void main(String args[]) {
        /* Class usage: */
        UrlWeather w = new UrlWeather("http://api.worldweatheronline.com/premium/v1/weather.ashx?",
                "b11152a3f5f44c859b6111012192601", "Szczecin", "3");
        w.Connect();
        JsonWeather w2 = new JsonWeather(JsonParser.ParseRawJson(w.GetResponse()));
        w2.DisplayDaysInfo(1);

        w.Disconnect();
    }
}