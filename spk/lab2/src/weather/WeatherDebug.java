package weather;

public class WeatherDebug {
    public static void DebugTrace() {
        String tmp = new WeatherConfig().GetConfigProperty("debug");
        if (tmp.equals("1")) {
            System.out.println("DBG - " + Thread.currentThread().getStackTrace()[2].getMethodName());
        }
    }
}