package weather;

import java.util.Properties;

public class WeatherConfig {
    private Properties _config;
    public String RawUrl, Key, Location, NumDays;

    public WeatherConfig() {
        _config = new Properties();
        try {
            _config.load(this.getClass().getClassLoader().getResourceAsStream("config.cfg"));
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Config file not found!");
        }
        RawUrl = GetConfigProperty("url");
        Key = GetConfigProperty("apiKey");
    }

    public String GetConfigProperty(String key) {
        try {
            return new String(_config.getProperty(key));
        } catch (NullPointerException e) {
            System.out.println("Property missing!");
        }
        return null;
    }

    public int ValidateData() {
        if (Location == null || NumDays == null || Integer.parseInt(NumDays) <= 0)
            return -1;
        else
            return 0;
    }

    public static void main(String[] args) {
        WeatherConfig conf = new WeatherConfig();
        System.out.println(conf.GetConfigProperty("apiKeyy"));
    }
}