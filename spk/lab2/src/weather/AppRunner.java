package weather;

import java.net.HttpURLConnection;
import java.net.URL;
import java.io.*;

import org.json.*;

public class AppRunner {
    public static void main(String[] args) {

        /* Read data from config file */
        WeatherConfig config = new WeatherConfig();
        WeatherCli cli = new WeatherCli(config, args);

        /* Establish connection */
        UrlWeather url = new UrlWeather(config.RawUrl, config.Key, config.Location, config.NumDays);
        url.Connect();

        /* Parse JSON response */
        JsonWeather json = new JsonWeather(JsonParser.ParseRawJson(url.GetResponse()));
        json.DisplayDaysInfo(Integer.parseInt(config.NumDays));

        url.Disconnect();
    }
}