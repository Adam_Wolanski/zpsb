package weather;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.*;

public class JsonParser {

    public static JSONObject ParseJsonFile(String filename) throws JSONException, IOException {
        WeatherDebug.DebugTrace();

        String rawJson = new String(Files.readAllBytes(Paths.get(filename)));
        return new JSONObject(rawJson);
    }

    public static JSONObject ParseRawJson(String rawJson) {
        WeatherDebug.DebugTrace();

        return new JSONObject(rawJson);
    }

    public static String GetMinTemperature(JSONObject obj) {
        WeatherDebug.DebugTrace();

        return obj.getString("mintempC");
    }

    public static String GetMaxTemperature(JSONObject obj) {
        WeatherDebug.DebugTrace();

        return obj.getString("maxtempC");
    }

    public static String GetDate(JSONObject obj) {
        WeatherDebug.DebugTrace();

        return obj.getString("date");
    }

    public static String GetSunset(JSONObject obj) {
        WeatherDebug.DebugTrace();

        return obj.getJSONArray("astronomy").getJSONObject(0).getString("sunset");
    }

    public static String GetSunrise(JSONObject obj) {
        WeatherDebug.DebugTrace();

        return obj.getJSONArray("astronomy").getJSONObject(0).getString("sunrise");
    }
}