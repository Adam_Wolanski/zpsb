package weather;

public class Day {
    public String Date;
    public String MaxTemperature;
    public String MinTemperature;
    public String SunriseTime;
    public String SunsetTime;

    public Day(String date, String minTemp, String maxTemp, String sunrise, String sunset) {
        Date = date;
        MinTemperature = minTemp;
        MaxTemperature = maxTemp;
        SunriseTime = sunrise;
        SunsetTime = sunset;
    }

    public void DisplayDayInfo() {
        WeatherDebug.DebugTrace();

        System.out.println("Date: " + Date);
        System.out.println("Sunrise: " + SunriseTime + " | Sunset: " + SunsetTime);
        System.out.println("Temperatures");
        System.out.println("    Max: " + MaxTemperature);
        System.out.println("    Min: " + MinTemperature + "\n");
    }
}