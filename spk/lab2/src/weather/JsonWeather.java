package weather;

import java.io.IOException;
import java.util.ArrayList;
import org.json.*;

public class JsonWeather {
    private ArrayList<Day> _days = new ArrayList<Day>();
    private JSONArray _jsonDays;

    JsonWeather(JSONObject json) {
        try {
            _jsonDays = json.getJSONObject("data").getJSONArray("weather");
            parseJsonData();
        } catch (JSONException e) {
            // TODO: handle exception
            System.out.println("Invalid location!");
            System.exit(-1);
        }
    }

    JsonWeather(String filename) {
        try {
            _jsonDays = JsonParser.ParseJsonFile(filename).getJSONObject("data").getJSONArray("weather");
            System.out.println(_jsonDays.length());
        } catch (IOException e) {
            System.out.println("File not found!");
        }
        parseJsonData();
    }

    private void parseJsonData() {
        WeatherDebug.DebugTrace();

        JSONObject it;
        for (int i = 0; i < _jsonDays.length(); ++i) {
            it = _jsonDays.getJSONObject(i);
            Day day = new Day(JsonParser.GetDate(it), JsonParser.GetMinTemperature(it),
                    JsonParser.GetMaxTemperature(it), JsonParser.GetSunrise(it), JsonParser.GetSunset(it));
            _days.add(day);
        }
    }

    public void DisplayDaysInfo(int numDays) {
        WeatherDebug.DebugTrace();

        if (_days.size() <= 0)
            System.out.println("Invalid number of days!");

        for (int i = 0; i < numDays && i < _days.size(); ++i) {
            _days.get(i).DisplayDayInfo();
        }
    }

    public void DisplayDaysInfo() {
        WeatherDebug.DebugTrace();

        for (int i = 0; i < _days.size(); ++i) {
            _days.get(i).DisplayDayInfo();
        }
    }

    public static void main(String[] args) {
        /* Class usage: */
        JsonWeather a = new JsonWeather("tmp1.json");
        a.DisplayDaysInfo();
        a.DisplayDaysInfo(a._days.size());
        a.DisplayDaysInfo(1);
    }
}