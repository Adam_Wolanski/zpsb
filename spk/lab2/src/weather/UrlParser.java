package weather;

import java.net.URL;
import java.io.*;

public class UrlParser {
    public static URL GetUrl(String url, String key, String location, String days) throws IOException {
        WeatherDebug.DebugTrace();

        String tmp = url + "key=" + key + "&q=" + location + "&format=json" + "&num_of_days=" + days;
        return new URL(tmp);
    }
}