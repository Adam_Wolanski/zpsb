package weather;

import gnu.getopt.*;

public class WeatherCli {

    private Getopt _getopt;

    public WeatherCli(WeatherConfig config, String[] args) {
        String arg;
        int c;
        boolean g = false, f = false;

        if (args.length <= 0) {
            cli_printHelp();
            System.exit(-1);
        }

        _getopt = new Getopt("weather", args, "-:l:g:d:h;");
        _getopt.setOpterr(false);

        while ((c = _getopt.getopt()) != -1) {
            switch (c) {
            case 'l':
                if (!f)
                    f = true;
                else
                    break;
                arg = _getopt.getOptarg();
                if (arg == null)
                    System.out.println("Missing location");
                config.Location = arg;
                break;
            case 'g':
                if (!f)
                    f = true;
                else
                    break;
                arg = _getopt.getOptarg();
                System.out.println(arg);
                break;
            case 'd':
                arg = _getopt.getOptarg();
                config.NumDays = arg;
                break;
            default:
            case ':':
            case '?':
            case 'h':
                g = true;
                cli_printHelp();
                if (config.ValidateData() == -1) {
                    System.exit(-1);
                }
                break;
            }
        }

        if (config.ValidateData() == -1 && g == false) {
            cli_printHelp();
            System.exit(-1);
        }

    }

    private void cli_printHelp() {
        System.out.println("--g : geolocation");
        System.out.println("--l : <required> city");
        System.out.println("--d : <required> number of days to print");
    }

    public void ProcessCli() {
    }

    public static void main(String[] args) {
        WeatherConfig config = new WeatherConfig();
        WeatherCli cli = new WeatherCli(config, args);
    }
}