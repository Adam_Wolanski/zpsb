package human;

public class Customer extends Person {
    private int _id;

    public Customer(String _name, String _lastName, int _age, int id) {
        super(_name, _lastName, _age);
        this._id = id;
    }

    public String toString() {
        return _name + ", " + _lastName + ", " + _age + ", " + _id;
    }
}