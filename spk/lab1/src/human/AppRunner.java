package human;

// import java.time.LocalDate;
// import java.time.LocalDateTime;
// import java.util.Calendar;

public class AppRunner {
    public static void main(String[] args) {
        // Calendar n = Calendar.getInstance();
        // n.set(Calendar.YEAR, 1950);
        // n.set(Calendar.MONTH, 10);
        // n.set(Calendar.DAY_OF_MONTH, 5);
        Person a = new Person("Jan", "Nowak", 123);
        Customer b = new Customer("Alex", "Nokak", 321, 10);

        // Person b = new Person("Jan", "Nowak", LocalDate.of(19S90, 12, 30));

        // System.out.println(Calendar.getInstance().getTime().getYear() -
        // a.GetAge().getYear());
        // System.out.println(a.GetName() + " " + a.GetLastName());

        System.out.println(a.toString());
        System.out.println(b.toString());
        // System.out.println(a.GetAllInfo());
    }
}