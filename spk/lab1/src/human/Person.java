package human;

// import java.util.Calendar;
// import java.util.Date;

public class Person {
    protected String _name, _lastName;
    protected int _age;

    Person(String name, String lname, int age) {
        _name = name;
        _lastName = lname;
        // Calendar n = Calendar.getInstance();
        // n.set(Calendar.YEAR, 1950);
        // n.set(Calendar.MONTH, 10);
        // n.set(Calendar.DAY_OF_MONTH, 5);
        _age = age;
    }

    public boolean equal(Object obj) {
        Person p = (Person) obj;
        return (_name == p._name) && (_lastName == p._lastName);
    }

    public String toString() {
        return _name + ", " + _lastName + ", " + _age;
    }

    public String GetAllInfo() {
        return _name + ", " + _lastName + ", " + _age;
    }

    public String GetName() {
        return _name;
    }

    public void SetName(String name) {
        _name = name;
    }

    public String GetLastName() {
        return _lastName;
    }

    public void SetLastname(String lastname) {
        _lastName = lastname;
    }

    public /* Date */int GetAge() {
        return _age;
        // return _age.getTime();
    }

    public void SetAge(/* Calendar */int age) {
        _age = age;
    }
}