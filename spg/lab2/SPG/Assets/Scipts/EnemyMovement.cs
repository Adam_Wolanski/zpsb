﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] private float _speed;
    private Transform _playerPosition;


	void Start ()
	{
        _playerPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
	}
	
	void Update ()
	{
	    if (!_playerPosition) return;
        transform.position = Vector3.MoveTowards(transform.position, _playerPosition.position, _speed * Time.deltaTime);
	}
}
