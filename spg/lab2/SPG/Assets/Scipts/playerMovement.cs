﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
	[SerializeField] private float _playerSpeed = 0;
    [SerializeField] private float _jumpForce = 0;

    private Vector3 _inputDirection;

    [SerializeField]
    private int score = 0;

	private void Start ()
	{
	    
	}
	
	private void Update () {
        _inputDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

        Vector3 normalizeVector = _inputDirection.normalized;

        transform.Translate(normalizeVector * _playerSpeed * Time.deltaTime);

	    if (Input.GetKeyDown("space"))
	    {
            Jump();
	    }
	}


    private void Jump()
    {
        GetComponent<Rigidbody>().AddForce(0, _jumpForce, 0);
    }

    public void Die()
    {
        CoinInteract.CoinAmount = 0;
    }

    private void OnDestroy()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //private void OnCollisionEnter(Collision col)
    //{
    //    Debug.Log("collision entered!");
    //    if (col.transform.tag == "Coin")
    //    {
    //        Destroy(col.gameObject);
    //        score++;
    //    }
    //}
}
