﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapInteract : MonoBehaviour {
	private void OnTriggerEnter(Collider col)
	{
	    if (col.CompareTag("Player"))
	    {
            col.GetComponent<PlayerMovement>().Die();
	        Destroy(col.gameObject);
	    }
	}
}
