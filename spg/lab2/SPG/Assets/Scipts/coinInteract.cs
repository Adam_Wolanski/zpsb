﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinInteract : MonoBehaviour
{
    public static int CoinAmount;

    private void Start()
    {
        CoinInteract.CoinAmount++;
        Debug.Log(CoinAmount);
    }

    private void OnTriggerEnter(Collider col)
    {
        Debug.Log("trigger entered!");
        if (!col.CompareTag("Player")) return;

        UpdateCoins();

        Destroy(this.gameObject);
    }

    private void UpdateCoins()
    {
        CoinInteract.CoinAmount--;
        if (CoinInteract.CoinAmount == 0)
        {
            Debug.Log("end");
        }
    }
}
