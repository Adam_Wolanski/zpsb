﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject particle;

    private void Start()
    {

    }

	void Update () {
	    FinishLevel();	
	}

    private void FinishLevel()
    {
        if (CoinInteract.CoinAmount == 0)
        {
            particle.GetComponent<ParticleSystem>().Play(true);
        }
    }

}
