﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float _smoothing = 3.0f;
    public Transform playerPosition;
    private Vector3 _offset;

    private float asd;

	void Start ()
	{
	    _offset = transform.position - playerPosition.position;
    }
	
	void Update ()
	{
	    transform.position = Vector3.Lerp(transform.position, playerPosition.position + _offset, _smoothing);
	}
}
