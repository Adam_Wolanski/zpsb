﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

	[SerializeField] private float _playerSpeed = 0.0f;
    [SerializeField] private float _playerJumpForce = 100.0f;
    [SerializeField] private Rigidbody _myRigidbody;


	// Use this for initialization
	private void Start ()
	{
        if (!_myRigidbody)
            _myRigidbody = GetComponent<Rigidbody>();
	}
	
	private void Update () {
		Move();
        Jump();
	    Look();
	}

    private void Move()
    {
        Vector3 _inputDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        transform.Translate(_inputDirection * _playerSpeed * Time.deltaTime);
    }

	private void Jump()
	{
        if (Input.GetKeyDown(KeyCode.Space))
		    _myRigidbody.AddForce(Vector3.up * _playerJumpForce);
	}

    private void Look()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            Vector3 lookPoint = new Vector3(hit.point.x, transform.position.y, hit.point.z);
            transform.LookAt(lookPoint);
        }
    }

}
