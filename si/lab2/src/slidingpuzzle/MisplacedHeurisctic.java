package slidingpuzzle;

import sac.*;

public class MisplacedHeurisctic extends StateFunction {
    public double calculate(State state) {
        int cnt = 0;
        SlidingPuzzle s = (SlidingPuzzle) state;
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                if (s.board[y][x] != s.boardBase[y][x])
                    cnt++;
            }
        }
        return cnt;
    }
}