package slidingpuzzle;

import slidingpuzzle.SlidingPuzzle.SlidingMoves;
import sac.graph.*;
import java.util.*;

public class AppRunner {
    public static void main(String[] args) {
        SlidingPuzzle puzzle = new SlidingPuzzle();

        puzzle.mixBoard(1000);

        SlidingPuzzle.setHFunction(new MisplacedHeurisctic());
        System.out.println("puzzle:\n" + puzzle);
        GraphSearchAlgorithm algorithm = new AStar(puzzle);
        algorithm.execute();
        SlidingPuzzle solution = (SlidingPuzzle) algorithm.getSolutions().get(0);
        System.out.println("moves: " + solution.getMovesAlongPath());
    }
}