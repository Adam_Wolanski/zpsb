package slidingpuzzle;

import sac.graph.*;
import java.util.*;

public class SlidingPuzzle extends GraphStateImpl {
    public final int rowSize = 3;
    private final int _boxSize = rowSize * rowSize;
    private Vector2 zeroPos;
    public int[][] board, boardBase;

    public SlidingPuzzle() {
        board = boardBase = new int[rowSize][rowSize];
        zeroPos = new Vector2(0, 0);
        int cnt = 0;
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < rowSize; j++) {
                board[i][j] = boardBase[i][j] = cnt++;
            }
        }
    }

    public SlidingPuzzle(SlidingPuzzle parent) {
        board = boardBase = new int[rowSize][rowSize];
        zeroPos = new Vector2(0, 0);
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < rowSize; j++) {
                board[i][j] = parent.board[i][j];
            }
        }
        zeroPos.posX = parent.zeroPos.posX;
        zeroPos.posY = parent.zeroPos.posY;
    }

    public boolean slideToDir(SlidingMoves dir) {
        switch (dir) {
        case UP:
            if (zeroPos.posY > 0) {
                int tmp = this.board[--zeroPos.posY][zeroPos.posX];
                this.board[zeroPos.posY][zeroPos.posX] = 0;
                this.board[zeroPos.posY + 1][zeroPos.posX] = tmp;
                return true;
            }
            break;
        case DOWN:
            if (zeroPos.posY < (rowSize - 1)) {
                int tmp = this.board[++zeroPos.posY][zeroPos.posX];
                this.board[zeroPos.posY][zeroPos.posX] = 0;
                this.board[zeroPos.posY - 1][zeroPos.posX] = tmp; // System.out.println(this.toString());
                return true;
            }
            break;
        case LEFT:
            if (zeroPos.posX > 0) {
                int tmp = this.board[zeroPos.posY][--zeroPos.posX];
                this.board[zeroPos.posY][zeroPos.posX] = 0;
                this.board[zeroPos.posY][zeroPos.posX + 1] = tmp;
                return true;
            }
            break;
        case RIGHT:
            if (zeroPos.posX < (rowSize - 1)) {
                int tmp = this.board[zeroPos.posY][++zeroPos.posX];
                this.board[zeroPos.posY][zeroPos.posX] = 0;
                this.board[zeroPos.posY][zeroPos.posX - 1] = tmp;
                return true;
            }
            break;
        }

        // System.out.println("[slideToDir]: Invalid move");
        return false;
    }

    public void mixBoard(int moveCount) {
        for (int i = 0; i < moveCount; i++) {
            slideToDir(getRandomMove());
        }
    }

    public static SlidingMoves getRandomMove() {
        Random rnd = new Random();
        SlidingMoves[] moves = SlidingMoves.values();
        return moves[rnd.nextInt(moves.length)];
    }

    public int hashCode() {
        return toString().hashCode();
    }

    // @Override
    public String toString() {
        String txt = "";
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < rowSize; j++) {
                txt += board[i][j] + ",";
            }
            txt += "\n";
        }
        return txt;
    }

    // @Override
    public boolean isSolution() {
        int cnt = 0;
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < rowSize; j++) {
                if (board[i][j] != cnt++) {
                    return false;
                }
            }
        }

        return true;
    }

    // @Override
    public List<GraphState> generateChildren() {
        List<GraphState> children = new ArrayList<GraphState>();

        // System.out.println("")
        for (SlidingMoves move : SlidingMoves.values()) {
            SlidingPuzzle child = new SlidingPuzzle(this);
            boolean retval;
            if (retval = (child.slideToDir(move) == true)) {
                children.add(child);
                child.setMoveName(move.getValue());
            }
        }

        return children;
    }

    public enum SlidingMoves {
        UP("UP"), DOWN("DOWN"), RIGHT("RIGHT"), LEFT("LEFT");
        private final String dir;

        SlidingMoves(String dir) {
            this.dir = dir;
        }

        public String getValue() {
            return dir;
        }
    }

    public class Vector2 {
        public int posX;
        public int posY;

        public Vector2(int x, int y) {
            posX = x;
            posY = y;
        }
    }
}

// sliding puzzle
// n = 3
// plansza [][]
// stale l,p,g,d,
// konstruktory
// tostring
// metoda wykonajruch(l,p,g,d) - zwrot bool
// metoda mieszajaca(liczba ruchow mieszajacych)
// h1 - zliczanie pol nie na miejscu
// h2 - manhattan/suma krokow kazdego pola do miejca docelowego abs((x2-x1) +
// (y2-y1))
// dziedziczenie po graphStateImpl
// isSolution
// generateChildren//child.setMoveName(l,p,g,d)
// hashcode
// wyswietlenie sciezki ruchow - solution.getPath() -> List<GraphState>
// alternatywnie solution.getMovesAlongPath() -> List<String>
// eksperyment zbiorczy - 100 ukladanek, 1000 mieszan * 2 herurystyki